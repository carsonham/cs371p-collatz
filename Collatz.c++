// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair

#include "Collatz.h"

using namespace std;

int cache[1000000];

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}


int cycle_length (long long i) {
    assert(i > 0);
    long long n = i;
    int count = 1;
    while (i != 1) {
        if (i < 1000000 && cache[i] != 0) {
            count += cache[i] - 1;
            i = 1;
        } else if (i % 2 == 0) {
            i /= 2;
            count++;
        } else {
            i = i + i/2 + 1; // does 2 steps in once, since the odd step always produces an even number
            count += 2;
        }
    }
    cache[n] = count;      // caches previously calculated cycle length values
    assert(count > 0);
    return count;
}

// ------------
// collatz_eval
// ------------


int collatz_eval (int i, int j) {
    assert(i <= j);
    int m = (j / 2) + 1;
    if (i < m) {
        i = m;      // mathematical optimization
    }
    int greatest = 0;
    for (int y = 0; y <= j - i; ++y) {
        int temp = cycle_length(y + i);
        if (temp > greatest) {
            greatest = temp;
        }
    }
    assert(greatest > 0);
    return greatest;
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    string s;
    while (getline(r, s)) {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
